# Calisthenics Community
O  projeto Calisthenics Community é uma ferramenta para auxiliar na organização de treinos e rotinas de exercícios físicos de forma simples e rápida.

[☁️ Google Cloud Deploy
☁️](https://calisthenicscommunity.rj.r.appspot.com)

## 🔎 Sobre
O objetivo do projeto é criar essa ferramente para auxiliar os usuários na organização de treinos, contando com os seguintes recursos:
- Criar rotinas;
- Consultar sua lista de rotinas;
- Atualizar rotinas já criadas;
- Apagar rotinas.


O sistema é totalmente baseado em usuários, ou seja, a partir do momento em que criar uma conta e criar suas rotinas poderá acessálas de qualquer dispositivo através do seu login.

É importante enfatizar que o projeto foi criado para a matéria *Desenvolvimento de Aplicações Web* do curos de *Engenharia de Computação* na *Faculdade de Engenharia de Sorocaba*.
##### Tecnologias utilizadas
- [Vue.js](https://vuejs.org/)
- [NodeJs](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)

##### Tópicos contemplados (segurança)
- [JWT](https://jwt.io/);
- *Password hashing* com [bcrypt](https://www.npmjs.com/package/bcrypt).

##### Capturas da aplicação
Algumas capturas das principais telas da aplicação.

![Screenshots](/doc/screenshots.png)

## 📖 Guias
Algumas intruções para rodar o projeto em local.
##### Requisitos
- [NodeJs](https://nodejs.org/en/download/)
- [npm](https://www.npmjs.com/get-npm)
- [MongoDB](https://docs.mongodb.com/manual/installation/)

##### Back end
Dentro do diretório */back-end* use:
``` bash
npm install
node index.js
```

##### Front end
Dentro do diretório */front-end* use:
``` bash
npm install
npm run serve
```

## 📝 Contribuidores
O projeto contou com as contribuições de:
- Bruno Faé Faion
- Arthur M. Brançam
