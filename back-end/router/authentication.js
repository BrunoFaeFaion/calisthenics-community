const express = require('express')
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const { User } = require('../models/schemas.js')
const bcrypt = require('bcrypt');

const saltRounds = 10;
const router = express.Router()
router.use(bodyParser.json());

router.post('/login', async (req, res) => {
  const user = await User.findOne({ email: req.body.email })
  if(user===null) res.send('User not found')
  console.log(user)
  console.log(`Req password => ${req.body.password}`)
  bcrypt.compare(req.body.password, user.password, async(err, result) => {
    if(result){
      jwt.sign({user}, 'secretkey', (err, token) => {
        res.send({
          token,
          user: {
            id: user['_id'],
            email: user.email,
            name: user.name
          }
        });
      });
    } else {
      res.send('Error ' + err)
    }
  });
});

router.post('/register', (req, res) => {
  console.log(req.body)
  bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
    const userJson = {
      name: req.body.name,
      email: req.body.email,
      password: hash
    }
    const user = User(userJson)
    jwt.sign({user}, 'secretkey', async (err, token) => {
      await user.save()
      userJson.id = user['_id']
      delete userJson.password
      res.send({
        token,
        user: userJson
      });
    });
  });
});

module.exports = router
