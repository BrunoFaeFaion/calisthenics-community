const { verifyToken } = require('../middlewares/authentication.js')
const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const { Workout } = require('../models/schemas.js')

const router = express.Router()
router.use(bodyParser.json());

router.get('/', verifyToken,(req, res) => {
  jwt.verify(req.token, 'secretkey', async (err, authData) => {
    if(err){
      res.sendStatus(403)
    } else {
      const query = await Workout.find( {userId: authData.user['_id']} ).exec()
      res.send(query);
    }
  });
});

router.get('/:id', verifyToken,(req, res) => {
  jwt.verify(req.token, 'secretkey', async (err, authData) => {
    if(err){
      res.sendStatus(403)
    } else {
      const query = await Workout.findById(req.params.id).exec()
      res.send(query);
    }
  });
});

router.post('/', verifyToken,(req, res) => {
  jwt.verify(req.token, 'secretkey', async (err, authData) => {
    if(err){
      res.sendStatus(403)
    } else {
      const workout = Workout(req.body)
      await workout.save()
      res.send({
        message: 'Welcome to the API!',
        workout
      });
    }
  });
});

router.delete('/:id', verifyToken,(req, res) => {
  jwt.verify(req.token, 'secretkey', async (err, authData) => {
    if(err){
      res.sendStatus(403)
    } else {
      await Workout.deleteOne({_id: req.params.id})
      res.send({
        message: 'Success!'
      });
    }
  });
});

router.put('/:id', verifyToken,(req, res) => {
  jwt.verify(req.token, 'secretkey', async (err, authData) => {
    if(err){
      res.sendStatus(403)
    } else {
      await Workout.update({_id: req.params.id}, { $set: req.body})
      res.send({
        message: 'Welcome to the API!'
      });
    }
  });
});

module.exports = router
