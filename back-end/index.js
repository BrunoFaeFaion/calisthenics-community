const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const { User } = require('./models/schemas.js')
const authentication = require('./router/authentication.js')
const workouts = require('./router/workouts.js')
const cors = require('cors');
require('dotenv').config()

const app = express();
const dbUrl = process.env.DB_URL == null ? 'mongodb://localhost/ccDev' : process.env.DB_URL
mongoose.connect(dbUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', '*');
    app.use(cors());
    next();
});

app.use('/auth', authentication)
app.use('/api/workouts', workouts)

app.listen(8081, () => console.log('App listening on port 8081'));
