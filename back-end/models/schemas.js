const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: '',
  name: '',
  password: ''
})

const workoutSchema = new mongoose.Schema({
  name: '',
  description: '',
  duration: 0,
  series: 0,
  difficulty: '',
  tags: [],
  exercises: [],
  userId: ''
})

const User = mongoose.model('User', userSchema);
const Workout = mongoose.model('Workout', workoutSchema);

module.exports = {
  User,
  Workout
}
