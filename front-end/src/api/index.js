import workouts from './modules/workouts.js'
import authentication from './modules/authentication.js'

export default {
  workouts,
  authentication
}
