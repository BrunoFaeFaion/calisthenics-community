const axios = require('axios');

module.exports = {
  login: (email, password) => {
    return axios.post('/auth/login', {
      email: email,
      password: password
    })
  },
  register: async (email, password, name) => {
    console.log(email)
    return axios.post('/auth/register', {
      name: name,
      email: email,
      password: password
    })
  }
}
