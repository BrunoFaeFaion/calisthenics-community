const axios = require('axios')

module.exports = {
  get: (payload) => {
    return mock[payload]
  },
  add: (payload) => {
    return axios.post('/api/workouts', payload)
  },
  update: (payload) => {
    return axios.put(`/api/workouts/${payload['_id']}`, payload)
  },
  getAll: () => {
    return axios.get('/api/workouts')
  },
  delete: (payload) => {
    return axios.delete(`/api/workouts/${payload}`)
  }
}

const mock = [
  {
    id: 0,
    title: 'Full body',
    description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    duration: 50,
    series: 5,
    difficulty: 'advanced',
    tags: ['full body','fat burning'],
    exercises: [
      {
        name: 'Planche',
        number: 50,
        unity: 'seconds'
      },
      {
        name: 'Push ups',
        number: 30,
        unity: 'seconds'
      },
      {
        name: 'Pull ups',
        number: 5,
        unity: 'units'
      },
      {
        name: 'Sit ups',
        number: 15,
        unity: 'units'
      },
    ]
  },
  {
    id: 1,
    title: 'Full body',
    description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    duration: 50,
    series: 5,
    difficulty: 'advanced',
    tags: ['full body','fat burning'],
    exercises: [
      {
        name: 'Planche',
        number: 50,
        unity: 'seconds'
      },
      {
        name: 'Push ups',
        number: 30,
        unity: 'seconds'
      },
      {
        name: 'Pull ups',
        number: 5,
        unity: 'units'
      },
      {
        name: 'Sit ups',
        number: 15,
        unity: 'units'
      },
    ]
  },
  {
    id: 2,
    title: 'Full body',
    description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    duration: 50,
    series: 5,
    difficulty: 'advanced',
    tags: ['full body','fat burning'],
    exercises: [
      {
        name: 'Planche',
        number: 50,
        unity: 'seconds'
      },
      {
        name: 'Push ups',
        number: 30,
        unity: 'seconds'
      },
      {
        name: 'Pull ups',
        number: 5,
        unity: 'units'
      },
      {
        name: 'Sit ups',
        number: 15,
        unity: 'units'
      },
    ]
  },
  {
    id: 3,
    title: 'Full body',
    description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    duration: 50,
    series: 5,
    difficulty: 'advanced',
    tags: ['full body','fat burning'],
    exercises: [
      {
        name: 'Planche',
        number: 50,
        unity: 'seconds'
      },
      {
        name: 'Push ups',
        number: 30,
        unity: 'seconds'
      },
      {
        name: 'Pull ups',
        number: 5,
        unity: 'units'
      },
      {
        name: 'Sit ups',
        number: 15,
        unity: 'units'
      },
    ]
  },
]
