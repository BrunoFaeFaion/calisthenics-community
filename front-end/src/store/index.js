import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api/index.js'
const axios = require('axios')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    workouts: [],
    user: {},
    token: localStorage.getItem('token') || ''
  },
  getters: {
    getWorkoutById: state => id => {
      return state.workouts.find(item => item["_id"] == id)
    },
    isLoggedIn: state => !!state.token,
  },
  mutations: {
    workouts(state, payload) {
      state.workouts = payload
    },
    auth_success(state, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.token}`
      state.token = payload.token
      state.user = payload.user
    },
    logout(state){
      state.token = ''
      state.user = ''
      state.workouts = []
      axios.defaults.headers.common['Authorization'] = ``
    },
    remove_workout(state, payload){
      state.workouts = state.workouts.filter(e => e['_id'] != payload)
    },
    update_workout(state, payload){
      let index = state.workouts.findIndex(e => e['_id'] == payload['_id'])
      state.workouts[index] = payload
    },
    push_workout(state, payload){
      state.workouts.push(payload)
    }
  },
  actions: {
    async fetchWorkouts({ commit }){
      const response = await api.workouts.getAll()
      commit('workouts', response.data)
    },

    async fetchWorkout({ commit }, payload){
      const workouts = await api.workouts.get(payload)
      commit('workouts', workouts)
    },
    async addWorkout({ commit, state }, payload){
      payload.userId = state.user.id
      const response = await api.workouts.add(payload)
      commit('push_workout', response.data.workout)
    },
    async deleteWorkout({ commit }, payload){
      await api.workouts.delete(payload)
      commit('remove_workout', payload)
    },
    async updateWorkout({ commit }, payload){
      await api.workouts.update(payload)
      commit('update_workout', payload)
    },
    async login({ commit }, payload){
      const response = await api.authentication.login(payload.email, payload.password)
      localStorage.setItem('token', response.data.token)
      commit('auth_success', {token: response.data.token, user: response.data.user})
    },
    async register({ commit }, payload){
      const response = await api.authentication.register(payload.email, payload.password, payload.name)
      commit('auth_success', {token: response.data.token, user: response.data.user})
    },
    logout({commit}){
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token')
        resolve()
      })
    }
  },
  modules: {
  }
})
